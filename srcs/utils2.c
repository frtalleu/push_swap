/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 14:01:37 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/23 14:01:38 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int
	size_of_stack(t_stack *st)
{
	int i;

	i = 0;
	while (st != NULL)
	{
		i++;
		st = st->next;
	}
	return (i);
}

t_min
	*get_two_min(t_stack *st, t_min *min)
{
	int i;

	min->inv_pos1 = 0;
	min->value1 = st->value;
	if (min->value1 == min->value)
		min->value1 = st->next->value;
	min->pos1 = 0;
	i = 0;
	while (st != NULL)
	{
		if (st->value <= min->value1 && st->value != min->value)
		{
			min->pos1 = i;
			min->value1 = st->value;
			min->inv_pos1 = 0;
		}
		else
			min->inv_pos1++;
		i++;
		st = st->next;
	}
	return (min);
}

int
	max_or_min_int(char **av, int ac)
{
	char	*str;
	int		i;

	i = 1;
	while (ac > i)
	{
		str = ft_itoa(ft_atoi(av[i]));
		if (ft_strncmp(str, av[i], ft_strlen(str)) != 0
			|| ft_strlen(str) != ft_strlen(av[i]))
		{
			free(str);
			return (0);
		}
		free(str);
		i++;
	}
	return (1);
}

t_min
	*get_min(t_stack *st, t_min *min)
{
	int		i;
	t_stack	*node;

	node = st;
	min->inv_pos = 0;
	min->value = st->value;
	min->pos = 0;
	i = 0;
	while (st != NULL)
	{
		if (st->value <= min->value)
		{
			min->pos = i;
			min->value = st->value;
			min->inv_pos = 0;
		}
		else
			min->inv_pos++;
		i++;
		st = st->next;
	}
	return (get_two_min(node, min));
}

char
	*do_instruct(char *str, char *to_add)
{
	char *tmp;

	tmp = ft_strjoin(str, to_add);
	free(str);
	return (tmp);
}
