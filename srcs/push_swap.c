/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 11:12:24 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/14 09:14:48 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

char
	*choose_algo(int ac, t_stacks *st, char **av)
{
	char	*str;
	t_stack	*st_sort;

	str = NULL;
	if (ac < 30)
	{
		str = ps_algo(st, str);
	}
	else if (ac < 300)
	{
		st_sort = sort_stack(init_stack(ac, av));
		st->chunks = get_chunks(st_sort, 20, malloc(sizeof(t_chunk)), NULL);
		str = algo_sort(st, str);
	}
	else
	{
		st_sort = sort_stack(init_stack(ac, av));
		st->chunks = get_chunks(st_sort, 51, malloc(sizeof(t_chunk)), NULL);
		str = algo_sort(st, str);
	}
	return (str);
}

int
	main(int ac, char **av)
{
	t_stacks	*st;
	char		*str;

	str = NULL;
	if (ac <= 2)
		return (0);
	st = malloc(sizeof(t_stacks));
	if (st == NULL)
		return (go_to_exit(st, "", 1));
	st->st_a = NULL;
	st->st_b = NULL;
	if (max_or_min_int(av, ac) == 0 || check_av(ac, av) == 0)
		return (go_to_exit(st, "Error\n", 2));
	st->st_a = init_stack(ac, av);
	if (check_double(st->st_a) == 0)
		return (go_to_exit(st, "Error\n", 2));
	st->st_b = init_stack(0, NULL);
	st->chunks = NULL;
	if (is_order(st->st_a) == 0)
		str = choose_algo(ac, st, av);
	ft_putstr_fd(str, 1);
	free_struct(st);
	free(str);
	return (0);
}
