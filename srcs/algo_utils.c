/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 15:11:16 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/23 15:11:17 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int
	is_in_chunk(t_stack *chunk, int value)
{
	while (chunk)
	{
		if (chunk->value == value)
			return (1);
		chunk = chunk->next;
	}
	return (0);
}

int
	all_is_sup(t_stack *st, int value)
{
	while (st)
	{
		if (value < st->value)
			return (0);
		st = st->next;
	}
	return (1);
}

int
	all_is_inf(t_stack *st, int value)
{
	while (st)
	{
		if (value > st->value)
			return (0);
		st = st->next;
	}
	return (1);
}

char
	*min_on_top(t_stacks *st, char *str)
{
	t_min *min;

	min = malloc(sizeof(t_min));
	if (min == NULL)
		return (NULL);
	min = get_min(st->st_b, min);
	while (min->value != st->st_b->value)
	{
		if (min->pos <= min->inv_pos)
		{
			str = do_instruct(str, "rb\n");
			st->st_b = rotate(st->st_b);
		}
		else
		{
			str = do_instruct(str, "rrb\n");
			st->st_b = reverse_rotate(st->st_b);
		}
	}
	free(min);
	return (str);
}

t_put_to_top
	*get_value(t_stack *st, int value)
{
	t_put_to_top	*top;
	int				i;

	top = malloc(sizeof(t_put_to_top));
	if (top == NULL)
		return (NULL);
	top->pos = 0;
	top->value = value;
	top->inv_pos = 0;
	i = 0;
	while (st)
	{
		if (st->value > value &&
				(st->value < top->value || top->value == value))
		{
			top->pos = i;
			top->value = st->value;
			top->inv_pos = 0;
		}
		else
			top->inv_pos++;
		i++;
		st = st->next;
	}
	return (top);
}
