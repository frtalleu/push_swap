/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 15:35:23 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/10 15:35:26 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"
#include "../libft/libft.h"

int
	go_to_exit(t_stacks *st, char *str, int exit)
{
	ft_putstr_fd(str, exit);
	if (st != NULL && st->st_a != NULL)
		free_stack(st->st_a);
	if (st != NULL && st->st_b != NULL)
		free_stack(st->st_b);
	if (st != NULL)
		free(st);
	return (0);
}

int
	check_double(t_stack *st)
{
	t_stack	*node;
	t_stack	*node1;
	int		i;

	node = st;
	node1 = st;
	while (node)
	{
		i = node->value;
		while (node1)
		{
			if (i == node1->value && node1 != node)
				return (0);
			node1 = node1->next;
		}
		node1 = st;
		node = node->next;
	}
	return (1);
}

t_stack
	*add_arg(t_stack *st, t_stack *node)
{
	t_stack	*res;

	res = st;
	while (st->next != NULL)
		st = st->next;
	st->next = node;
	return (res);
}

t_stack
	*init_stack(int ac, char **av)
{
	t_stack	*st;
	t_stack	*node;
	int		i;

	st = NULL;
	i = 1;
	while (i < ac && ac != 1)
	{
		node = malloc(sizeof(t_stack));
		if (!(node))
			return (NULL);
		node->value = ft_atoi(av[i]);
		node->next = NULL;
		if (st == NULL)
			st = node;
		else
			st = add_arg(st, node);
		i++;
	}
	return (st);
}

int
	check_av(int ac, char **av)
{
	int	i;
	int	j;

	i = 1;
	while (ac > i)
	{
		j = 0;
		if (av[i][0] == '\0')
			return (0);
		while (av[i][j] != '\0')
		{
			if (av[i][j] == '-' && j != 0)
				return (0);
			if (ft_isdigit(av[i][j]) == 0 && av[i][j] != '-')
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}
