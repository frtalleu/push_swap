/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_instruct.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 13:49:07 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/10 13:49:14 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"
#include "../libft/libft.h"

t_stacks
	*double_reverse(t_stacks *st)
{
	st->st_b = reverse_rotate(st->st_b);
	st->st_a = reverse_rotate(st->st_a);
	return (st);
}

t_stacks
	*double_rotate(t_stacks *st)
{
	st->st_b = rotate(st->st_b);
	st->st_a = rotate(st->st_a);
	return (st);
}

t_stacks
	*double_swap(t_stacks *st)
{
	st->st_b = swap(st->st_b);
	st->st_a = swap(st->st_a);
	return (st);
}
