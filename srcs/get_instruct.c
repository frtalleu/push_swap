/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_instruct.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 14:04:22 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/10 14:04:24 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include "../includes/push_swap.h"

t_stacks
	*get_swap(char *op, t_stacks *st)
{
	if (ft_strlen(op) == 2 && op[1] == 'a')
		st->st_a = swap(st->st_a);
	else if (ft_strlen(op) == 2 && op[1] == 'b')
		st->st_b = swap(st->st_b);
	else if (ft_strlen(op) == 2 && op[1] == 's')
		st = double_swap(st);
	else
		st->err = 1;
	return (st);
}

t_stacks
	*get_push(char *op, t_stacks *st)
{
	if (ft_strlen(op) == 2 && op[1] == 'a')
		st = push_a(st);
	else if (ft_strlen(op) == 2 && op[1] == 'b')
		st = push_b(st);
	else
		st->err = 1;
	return (st);
}

t_stacks
	*get_rotate(char *op, t_stacks *st)
{
	if (ft_strlen(op) == 2 && op[1] == 'a')
		st->st_a = rotate(st->st_a);
	else if (ft_strlen(op) == 2 && op[1] == 'b')
		st->st_b = rotate(st->st_b);
	else if (ft_strlen(op) == 2 && op[1] == 'r')
		st = double_rotate(st);
	else if (ft_strlen(op) == 3 && op[1] == 'r' && op[2] == 'a')
		st->st_a = reverse_rotate(st->st_a);
	else if (ft_strlen(op) == 3 && op[1] == 'r' && op[2] == 'b')
		st->st_b = reverse_rotate(st->st_b);
	else if (ft_strlen(op) == 3 && op[1] == 'r' && op[2] == 'r')
		st = double_reverse(st);
	else
		st->err = 1;
	return (st);
}

t_stacks
	*get_op(char *op, t_stacks *st)
{
	if (ft_strlen(op) >= 2)
		if (op[0] == 's')
			st = get_swap(op, st);
		else if (op[0] == 'p')
			st = get_push(op, st);
		else if (op[0] == 'r')
			st = get_rotate(op, st);
		else
			st->err = 1;
	else
		st->err = 1;
	return (st);
}
