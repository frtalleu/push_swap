/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 13:11:13 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/05 13:11:15 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"
#include "../libft/libft.h"

char
	*check_exit(t_stacks *st, int ac)
{
	int		i;
	t_stack	*stack;

	if (st->st_b != NULL)
		return ("KO\n");
	if (ac == 2)
		return ("OK\n");
	stack = st->st_a;
	i = stack->value;
	stack = stack->next;
	while (stack)
	{
		if (i > stack->value)
			return ("KO\n");
		i = stack->value;
		stack = stack->next;
	}
	return ("OK\n");
}

int
	main(int ac, char **av)
{
	t_stacks	*st;
	char		*str;

	if (ac == 1)
		return (0);
	st = malloc(sizeof(t_stacks));
	st->st_a = NULL;
	st->st_b = NULL;
	if (max_or_min_int(av, ac) == 0 || check_av(ac, av) == 0)
		return (go_to_exit(st, "Error\n", 2));
	if (st == NULL)
		return (0);
	st->st_a = init_stack(ac, av);
	if (check_double(st->st_a) == 0)
		return (go_to_exit(st, "Error\n", 2));
	st->err = 0;
	while (get_next_line(0, &str, 0) > 0)
	{
		st = get_op(str, st);
		if (st->err != 0)
			return (go_to_exit(st, "Error\n", 2));
		free(str);
	}
	free(str);
//	get_next_line(0, &str, 1);
	go_to_exit(st, check_exit(st, ac), 1);
}
