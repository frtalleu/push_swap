/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_utils2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 15:21:10 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/23 15:21:11 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

char
	*good_rotate_b(t_stacks *st, char *str, int value)
{
	t_put_to_top *top;

	top = get_value(st->st_b, value);
	while (top->value != st->st_b->value)
	{
		if (top->pos <= top->inv_pos)
		{
			str = do_instruct(str, "rb\n");
			st->st_b = rotate(st->st_b);
		}
		else
		{
			str = do_instruct(str, "rrb\n");
			st->st_b = reverse_rotate(st->st_b);
		}
	}
	free(top);
	return (str);
}

char
	*prepare_b(int value, t_stacks *st, char *str)
{
	int				inf;
	int				sup;

	if (st->st_b == NULL || size_of_stack(st->st_b) == 1)
	{
		str = do_instruct(str, "pb\n");
		st = push_b(st);
		return (str);
	}
	inf = all_is_inf(st->st_b, value);
	sup = all_is_sup(st->st_b, value);
	if (inf || sup)
		str = min_on_top(st, str);
	else
		str = good_rotate_b(st, str, value);
	str = do_instruct(str, "pb\n");
	st = push_b(st);
	return (str);
}

int
	all_current_chunk_load(t_chunk *chk, t_stack *st)
{
	t_stack *chunk;
	t_stack *node;

	chunk = chk->chunk;
	node = st;
	while (chunk)
	{
		while (node)
		{
			if (node->value == chunk->value)
				break ;
			node = node->next;
		}
		if (node == NULL)
			return (0);
		node = st;
		chunk = chunk->next;
	}
	return (1);
}

int
	last_elements(t_stack *st)
{
	int i;

	while (st)
	{
		i = st->value;
		st = st->next;
	}
	return (i);
}

char
	*prepare_a_for_push(char *str, t_chunk *chunk, t_stacks *st)
{
	while (chunk != NULL &&
	is_in_chunk(chunk->chunk, last_elements(st->st_a)) == 0)
	{
		str = do_instruct(str, "rra\n");
		st->st_a = reverse_rotate(st->st_a);
	}
	return (str);
}
