/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repush_b.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 14:45:04 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/23 14:45:05 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

char
	*repush_b2(t_stacks *st, char *str)
{
	while (size_of_stack(st->st_b) != 0)
	{
		str = do_instruct(str, "pa\n");
		st = push_a(st);
	}
	return (str);
}

char
	*repush_b(t_stacks *st, char *str)
{
	while (size_of_stack(st->st_b) != 0)
	{
		str = do_instruct(str, "pa\n");
		st = push_a(st);
		str = do_instruct(str, "ra\n");
		st->st_a = rotate(st->st_a);
	}
	return (str);
}

int
	is_order(t_stack *st)
{
	int		i;
	t_stack	*node;

	i = st->value;
	node = st->next;
	while (node)
	{
		if (i > node->value)
			return (0);
		i = node->value;
		node = node->next;
	}
	return (1);
}
