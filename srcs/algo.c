/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 15:29:06 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/23 15:29:10 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

char
	*ps_algo(t_stacks *st, char *str)
{
	t_min *min;

	min = malloc(sizeof(t_min));
	while (size_of_stack(st->st_a) > 3 && is_order(st->st_a) == 0)
	{
		min = get_min(st->st_a, min);
		if (min->pos == 0)
		{
			str = do_instruct(str, "pb\n");
			st = push_b(st);
		}
		else if (min->pos <= min->inv_pos)
		{
			str = do_instruct(str, "ra\n");
			st->st_a = rotate(st->st_a);
		}
		else
		{
			str = do_instruct(str, "rra\n");
			st->st_a = reverse_rotate(st->st_a);
		}
	}
	str = finish_it(st, str);
	free(min);
	return (repush_b2(st, str));
}

char
	*min_on_top_a(t_stacks *st, char *str)
{
	t_min *min;

	min = malloc(sizeof(t_min));
	if (min == NULL)
		return (NULL);
	min = get_min(st->st_a, min);
	while (min->value != st->st_a->value)
	{
		if (min->pos <= min->inv_pos)
		{
			str = do_instruct(str, "rb\n");
			st->st_a = rotate(st->st_a);
		}
		else
		{
			str = do_instruct(str, "rrb\n");
			st->st_a = reverse_rotate(st->st_a);
		}
	}
	free(min);
	return (str);
}

char
	*algo_sort(t_stacks *st, char *str)
{
	t_chunk *current_chunk;
	t_chunk *node;

	node = NULL;
	current_chunk = st->chunks;
	while (current_chunk != NULL)
		if (is_in_chunk(current_chunk->chunk, st->st_a->value) == 1)
		{
			str = prepare_b(st->st_a->value, st, str);
		}
		else if (all_current_chunk_load(current_chunk, st->st_b) == 1)
		{
			str = prepare_a_for_push(str, node, st);
			if (size_of_stack(st->st_b) > 1)
				str = min_on_top(st, str);
			str = repush_b(st, str);
			node = current_chunk;
			current_chunk = current_chunk->next;
		}
		else
		{
			str = do_instruct(str, "ra\n");
			st->st_a = rotate(st->st_a);
		}
	return (min_on_top_a(st, str));
}

t_chunk
	*get_chunks(t_stack *st, int size, t_chunk *lst_chunk, t_chunk *first)
{
	int i;

	if (lst_chunk == NULL)
		return (NULL);
	first = lst_chunk;
	while (st)
	{
		i = 0;
		lst_chunk->chunk = st;
		while (st && i++ < size - 1)
			st = st->next;
		if (st)
		{
			lst_chunk->next = malloc(sizeof(t_chunk));
			if (lst_chunk == NULL)
				return (NULL);
			lst_chunk = lst_chunk->next;
			lst_chunk->chunk = st->next;
			st->next = NULL;
			st = lst_chunk->chunk;
		}
	}
	lst_chunk->next = NULL;
	return (first);
}

t_stack
	*sort_stack(t_stack *st)
{
	t_stack *node;
	t_stack *node1;
	t_stack *prev;

	node1 = st;
	node = node1->next;
	while (node1 && node1->next != NULL)
		if (node1->value > node->value)
		{
			if (node1->value == st->value)
				st = swap(st);
			else
				prev->next = swap(node1);
			node1 = st;
			node = node1->next;
		}
		else
		{
			prev = node1;
			node1 = node1->next;
			node = node->next;
		}
	return (st);
}
