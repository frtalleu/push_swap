/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruct.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 13:32:11 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/10 13:32:30 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include "../includes/push_swap.h"

t_stack
	*swap(t_stack *st)
{
	t_stack *st1;
	t_stack *st2;

	if (st == NULL || st->next == NULL)
		return (st);
	st1 = st;
	st2 = st->next;
	st1->next = st2->next;
	st2->next = st1;
	return (st2);
}

t_stack
	*rotate(t_stack *st)
{
	t_stack *node;
	t_stack *node1;

	if (st == NULL || st->next == NULL)
		return (st);
	node = st->next;
	node1 = st;
	while (st->next != NULL)
		st = st->next;
	node1->next = NULL;
	st->next = node1;
	return (node);
}

t_stack
	*reverse_rotate(t_stack *st)
{
	t_stack *node;
	t_stack *node1;

	if (st == NULL || st->next == NULL)
		return (st);
	node = st;
	while (st->next)
	{
		node1 = st;
		st = st->next;
	}
	node1->next = NULL;
	st->next = node;
	return (st);
}

t_stacks
	*push_a(t_stacks *st)
{
	t_stack *st_a;
	t_stack *st_b;
	t_stack *node;

	if (st->st_b == NULL)
		return (st);
	st_a = st->st_a;
	st_b = st->st_b;
	node = st_b;
	st_b = st_b->next;
	node->next = st_a;
	st_a = node;
	st->st_b = st_b;
	st->st_a = st_a;
	return (st);
}

t_stacks
	*push_b(t_stacks *st)
{
	t_stack *node;

	node = st->st_b;
	st->st_b = st->st_a;
	st->st_a = node;
	st = push_a(st);
	node = st->st_b;
	st->st_b = st->st_a;
	st->st_a = node;
	return (st);
}
