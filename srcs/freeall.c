/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   freeall.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 13:53:47 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/23 13:53:49 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void
	free_stack(t_stack *st)
{
	t_stack	*node;

	while (st)
	{
		node = st->next;
		free(st);
		st = node;
	}
}

void
	free_chunks(t_chunk *st)
{
	t_chunk *node;

	node = st;
	while (st)
	{
		node = st->next;
		free_stack(st->chunk);
		free(st);
		st = node;
	}
}

void
	free_struct(t_stacks *st)
{
	if (st->st_a)
		free_stack(st->st_a);
	if (st->st_b)
		free_stack(st->st_b);
	if (st->chunks)
		free_chunks(st->chunks);
	free(st);
}
