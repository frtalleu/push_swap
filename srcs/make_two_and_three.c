/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_two_and_three.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/23 14:15:55 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/23 14:15:57 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

char
	*make_three2(t_stacks *st, char *str, int pos_2, int pos_3)
{
	int pos_1;

	pos_1 = st->st_a->value;
	if (pos_1 > pos_2 && pos_1 < pos_3 && pos_2 < pos_3)
	{
		str = do_instruct(str, "sa\n");
		st->st_a = swap(st->st_a);
	}
	else if (pos_1 > pos_2 && pos_1 > pos_3 && pos_2 < pos_3)
	{
		str = do_instruct(str, "ra\n");
		st->st_a = rotate(st->st_a);
	}
	else if (pos_1 > pos_2 && pos_1 > pos_3 && pos_2 > pos_3)
	{
		str = do_instruct(str, "sa\n");
		st->st_a = swap(st->st_a);
		str = do_instruct(str, "rra\n");
		st->st_a = reverse_rotate(st->st_a);
	}
	return (str);
}

char
	*make_three(t_stacks *st, char *str)
{
	int pos_1;
	int pos_2;
	int pos_3;

	pos_1 = st->st_a->value;
	pos_2 = st->st_a->next->value;
	pos_3 = st->st_a->next->next->value;
	if (pos_1 < pos_2 && pos_1 < pos_3 && pos_2 > pos_3)
	{
		str = do_instruct(str, "rra\n");
		st->st_a = reverse_rotate(st->st_a);
		str = do_instruct(str, "sa\n");
		st->st_a = swap(st->st_a);
	}
	else if (pos_1 < pos_2 && pos_1 > pos_3 && pos_2 > pos_3)
	{
		str = do_instruct(str, "rra\n");
		st->st_a = reverse_rotate(st->st_a);
	}
	else
		str = make_three2(st, str, pos_2, pos_3);
	return (str);
}

char
	*make_two(t_stacks *st, char *str)
{
	t_stack *node;

	node = st->st_a->next;
	if (st->st_a->value > node->value)
	{
		str = do_instruct(str, "sa\n");
		st->st_a = swap(st->st_a);
	}
	return (str);
}

char
	*finish_it(t_stacks *st, char *str)
{
	int i;

	i = size_of_stack(st->st_a);
	if (i == 3)
		str = make_three(st, str);
	else if (i == 2)
		str = make_two(st, str);
	return (str);
}
