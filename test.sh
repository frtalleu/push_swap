#!/bin/bash --posix

if [ -e trace_5 ] ; then
	rm trace_5
fi

if [ -e trace_100 ] ; then
	rm trace_100
fi

if [ -e trace_500 ] ; then
	rm trace_500
fi

make > /dev/null

echo "TEST WRONG ARG"
echo ""
echo ""
echo "non numeric arg"
echo ""
echo "./checker 82 17 27 91 47 sd 3289 237"
./checker 82 17 27 91 47 sd 3289 237
echo ""
echo ""
echo "double number"
echo ""
echo "./checker 182 290 37 10 3732 327 10"
./checker 182 290 37 10 3732 327 10
echo ""
echo ""
echo "owerflow"
echo ""
echo "./checker 2147483648"
./checker 2147483648
echo ""
echo "./checker -2147483649"
./checker -2147483649
echo ""
echo ""
echo "whithout parameter"
echo "./checker"
./checker
echo ""
echo ""
echo "wrong instruction"
echo "rae" > a
echo ""
echo "./checker 0 1 23 6 whith 'rae' for instruction"
cat a | ./checker 0 1 23 6
rm a
echo ""
echo "       ra      " > a
echo "./checker 0 1 23 6 with      '       ra      ' for instruction"
cat a | ./checker 0 1 23 6
echo ""
echo ""
rm a
echo "VALIDITY OF CHECKER"
echo ""
echo "./checker 0 1 9 8 2 7 3 6 4 5 whith [sa, pb, rrr] for instruct"
echo sa > a; echo pb > a ; echo rrr > a
cat a | ./checker 0 1 9 8 2 7 3 6 4 5
echo ""
echo "./checker 0 1 2 without instruction"
rm a
touch a
cat a | ./checker 0 1 2
echo ""
rm a
echo "./checker 0 9 1 8 2 with [pb, ra, pb, ra, sa ra, pa ,pa]"
echo pb >> a; echo ra >> a; echo pb >> a; echo ra >> a; echo sa >> a; echo ra >> a; echo pa >> a; echo pa >> a
cat a | ./checker 0 9 1 8 2
echo ""
echo ""
echo "TEST PUSH_SWAP"
echo ""
echo ""
echo "test with sort number"
echo ""
echo "./push_swap 42"
rm a
./push_swap 42 > a
echo "number of instruct"
cat a | wc -l
cat a | ./checker 42 
echo ""
echo "./push_swap 0 1 2 3"
rm a
./push_swap 0 1 2 3 > a
echo "number of instruct"
cat a | wc -l
cat a | ./checker 0 1 2 3 5 
echo ""
echo "./push_swap 0 1 2 3 4 5 6 7 8 9"
rm a
./push_swap 0 1 2 3 4 5 6 7 8 9 > a
echo "number of instruct"
cat a | wc -l
cat a | ./checker 0 1 2 3 5 6 7 8 9
echo ""
echo ""
echo "test push_swap 5 number"
echo "result in trace_5"
export i=0
while [ $i -le 10 ]; do 
	export ARG=`ruby -e "puts (1...5).to_a.shuffle.join(' ')"`
	./push_swap $ARG > a
	cat a | wc -l >> trace_5
	./push_swap $ARG | ./checker $ARG >> trace_5
	echo $ARG >> trace_5
	rm a
	i=$((i+1))
done
echo ""
echo ""
echo "test push_swap 100 number"
echo "result in trace_100"
export i=0
while [ $i -le 10 ]; do 
	export ARG=`ruby -e "puts (1...100).to_a.shuffle.join(' ')"`
	./push_swap $ARG > a
	cat a | wc -l >> trace_100
	./push_swap $ARG | ./checker $ARG >> trace_100
	echo $ARG >> trace_100
	rm a
	i=$((i+1))
done
echo ""
echo ""
echo "test push_swap 500 number"
echo "result in trace_500"
export i=0
while [ $i -le 10 ]; do 
	export ARG=`ruby -e "puts (1...500).to_a.shuffle.join(' ')"`
	./push_swap $ARG > a
	cat a | wc -l >> trace_500
	./push_swap $ARG | ./checker $ARG >> trace_500
	echo $ARG >> trace_500
	rm a
	i=$((i+1))
done

make fclean > /dev/null
