# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/03/06 13:44:38 by frtalleu          #+#    #+#              #
#    Updated: 2021/03/06 13:44:39 by frtalleu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRCS = ./srcs/checker.c ./gnl/get_next_line.c ./gnl/get_next_line_utils.c \
		./srcs/instruct.c ./srcs/double_instruct.c ./srcs/get_instruct.c \
		./srcs/utils.c ./srcs/freeall.c ./srcs/utils2.c

SRC = ./srcs/push_swap.c \
		./srcs/instruct.c ./srcs/double_instruct.c ./srcs/get_instruct.c \
		./srcs/utils.c ./srcs/freeall.c ./srcs/utils2.c ./srcs/make_two_and_three.c\
		./srcs/repush_b.c ./srcs/algo_utils.c ./srcs/algo_utils2.c ./srcs/algo.c

OBJS = ${SRCS:.c=.o}

OBJ = ${SRC:.c=.o}

FLAGS = -Wall -Wextra -Werror 

.c.o:
	gcc $(FLAGS) -c $< -o $(<:.c=.o)

NAME = checker
NAME1 = push_swap

$(NAME): $(OBJS) $(OBJ)
	$(MAKE) -C ./libft
	gcc $(FLAGS) -o $(NAME) $(OBJS) ./libft/libft.a 
	gcc $(FLAGS) -o $(NAME1) $(OBJ) ./libft/libft.a

clean:
	$(MAKE) -C ./libft/. clean
	rm -f $(OBJS)
	rm -f $(OBJ)

fclean: clean
	$(MAKE) -C ./libft/. clean
	rm -f $(NAME)
	rm -f $(NAME1)
	rm -f libft/libft.a

re: fclean $(NAME)

.PHONY: clean fclean re
