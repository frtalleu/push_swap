/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/05 13:12:49 by frtalleu          #+#    #+#             */
/*   Updated: 2021/03/14 09:14:53 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _PUSH_SWAP_H
# define _PUSH_SWAP_H

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include "../gnl/get_next_line.h"
# include "../libft/libft.h"

typedef struct	s_stack
{
	int				value;
	struct s_stack	*next;
}				t_stack;

typedef struct	s_chunk
{
	t_stack			*chunk;
	struct s_chunk	*next;
}				t_chunk;

typedef struct	s_stacks
{
	t_stack		*st_a;
	t_stack		*st_b;
	t_chunk		*chunks;
	int			err;
}				t_stacks;

typedef struct	s_put_to_top
{
	int value;
	int pos;
	int inv_pos;
}				t_put_to_top;

typedef struct	s_min
{
	int value1;
	int pos1;
	int inv_pos1;
	int value;
	int pos;
	int inv_pos;
}				t_min;

int				max_or_min_int(char **av, int ac);
char			*algo_sort(t_stacks *st, char *str);
char			*min_on_top_a(t_stacks *st, char *str);
char			*ps_algo(t_stacks *st, char *str);
t_stack			*sort_stack(t_stack *st);
t_chunk			*get_chunks(t_stack *st, int size, t_chunk *lst_chunk,
				t_chunk *first);
char			*algo_sort(t_stacks *st, char *str);
char			*min_on_top_a(t_stacks *st, char *str);
char			*ps_algo(t_stacks *st, char *str);
char			*prepare_a_for_push(char *str, t_chunk *chunk, t_stacks *st);
int				last_element(t_stack *st);
int				all_current_chunk_load(t_chunk*chk, t_stack *st);
char			*prepare_b(int value, t_stacks *st, char *str);
char			*good_rotate_b(t_stacks *st, char *str, int value);
t_put_to_top	*get_value(t_stack *st, int value);
char			*min_on_top(t_stacks *st, char *str);
int				all_is_inf(t_stack *st, int value);
int				all_is_sup(t_stack *st, int value);
int				is_in_chunk(t_stack *chunk, int value);
char			*repush_b2(t_stacks *st, char *str);
char			*repush_b(t_stacks *st, char *str);
int				is_order(t_stack *st);
char			*finish_it(t_stacks *st, char *str);
char			*make_two(t_stacks *st, char *str);
char			*make_three(t_stacks *st, char *str);
char			*make_three2(t_stacks *st, char *str, int pos_2, int pos3);
int				size_of_stack(t_stack *st);
t_min			*get_two_min(t_stack *st, t_min *min);
t_min			*get_min(t_stack *st, t_min *min);
char			*do_instruct(char *str, char *to_add);
void			free_struct(t_stacks *st);
void			free_chunks(t_chunk *st);
int				go_to_exit(t_stacks *st, char *str, int exit);
t_stack			*swap(t_stack *st);
t_stack			*rotate(t_stack *st);
t_stack			*reverse_rotate(t_stack *st);
t_stacks		*push_a(t_stacks *st);
t_stacks		*double_reverse(t_stacks *st);
t_stacks		*double_rotate(t_stacks *st);
t_stacks		*double_swap(t_stacks *st);
t_stacks		*push_b(t_stacks *st);
t_stacks		*get_swap(char *op, t_stacks *st);
t_stacks		*get_push(char *op, t_stacks *st);
t_stacks		*get_rotate(char *op, t_stacks *st);
t_stacks		*get_op(char *op, t_stacks *st);
void			free_stack(t_stack *st);
int				check_double(t_stack *st);
t_stack			*add_arg(t_stack *st, t_stack *node);
t_stack			*init_stack(int ac, char **av);
int				check_av(int ac, char **av);
#endif
